﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
namespace Demo.Domain.Entities
{
    [Table("KhachHang")]
    public class KhachHang
    {
        [Key]
        public int MaKhachHang { get; set; }
        public string TenKhachHang { get; set; }
        public string Email {get; set;}
        public string DienThoai { get; set; }
        public string DiaChi { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
namespace Demo.Domain.Entities
{
    [Table("DonHang")]
    public class DonHang
    {
        [Key]
        public int MaDonHang { get; set; }
        public int MaKhachHang { get; set; }
        public string GhiChu { get; set; }
        public bool Ship { get; set; }
        public DateTime NgayTao { get; set; }
    }
}
